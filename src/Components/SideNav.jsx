import React from 'react'
import{ useState } from "react";
import {links} from './NavLink'
import { Link } from 'react-scroll';
import { AiFillYoutube,AiFillInstagram,AiFillTwitterSquare } from "react-icons/ai";
import { HiOutlineMail } from "react-icons/hi";
const SideNav = () => {
  const slink = [
    {
      id: 1,
      child: (
        <>
          Twitter <AiFillTwitterSquare size={30} />
        </>
      ),
      href: "https://twitter.com/anoopsivakumar7",
      style: "rounded-tr-md",
    },
    {
      id: 2,
      child: (
        <>
          Instagram <AiFillInstagram size={30} />
        </>
      ),
      href: "https://instagram.com/magiccoverzz",
    },
    {
      id: 3,
      child: (
        <>
          Youtube <AiFillYoutube size={30} />
        </>
      ),
      href: "https://www.youtube.com/@MagicCoverzzTechAndTips",
    },
    {
      id: 4,
      child: (
        <>
          Mail <HiOutlineMail size={30} />
        </>
      ),
      href: "magiccoverzz@gmail.com",
    },
    {
      id: 5,
      child: (
        <>
           Personal IG <AiFillInstagram size={30} />
        </>
      ),
      href: "https://instagram.com/anoopsivakumar10",
    },
  ];
    const [nav,setNav]= useState(false)
  return (
   
        <div className='flex flex-col justify-center items-center absolute top-0 left-0 w-80  h-screen bg-gradient-to-b from-black to-gray-800 text-gray-400 ease-in'>
        <ul className='flex flex-col absolute  w-60  top-20 ease-in' >
        {links.map(({id,link}) =>(
            <li key={id} className='flex items-center capitalize p-2 text-base  font-normal text-gray-400 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700'><Link onClick={()=>setNav(!nav)} to={link} smooth duration={500}>{link}</Link></li>
            ))}
        </ul>
       <div className='mt-9'>
      
       <div className=" flex top-[60%] left-0 fixed ">
      <ul>
        {slink.map((link) => (
          <li
            key={link.id}
            className={
              "flex justify-between items-center w-40 h-14 px-4 ml-[-100px] hover:ml-[-10px] hover:rounded-md duration-300 bg-gray-500" +
              " " +
              link.style
            }
          >
            <a
              href={link.href ? link.href : "/"}
              className="flex justify-between items-center w-full text-white"
              download={link.download}
              target="_blank"
              rel="noreferrer"
            >
              {link.child}
            </a>
          </li>
        ))}
      </ul>
    </div> 




       </div>
    </div>
    
   
    
  )
}

export default SideNav