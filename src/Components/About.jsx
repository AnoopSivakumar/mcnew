import React from 'react'
import HeroImage from "../assets/mclogo.png";
const About = () => {
  return (

    <>
    <div
    name="about"
    className="w-full   bg-opacity-50 h-screen bg-gradient-to-b from-gray-800 to-black text-white"
  >
    
    <div className="  max-w-screen-lg p-4 mx-auto flex flex-col justify-center w-full h-full">
      <div className="pb-2">
        <p className="text-4xl font-bold inline border-b-4 border-gray-500">
          About
        </p>
      </div>
      <div className='max-w-screen-lg mx-auto flex flex-col md:flex-row items-center justify-center h-full px-4'>
      
        
      <img
            src={HeroImage}
            alt="my profile"
            className="rounded-2xl mx-auto w-2/3 md:w-full"
      />  
      
      <p className="text-2xl">
      Hi, I Am Anoop Sivakumar, A Video Creator Here For You To Explore. 
        I Make Informative Tech Tutorial Videos On My Magic Coverzz Tech And Tips Youtube Channel .
       I Love Having The Opportunity To Share My Passions And Thoughts With My Viewers.
      </p>
     
      </div>
      
    </div>
  </div>
  </>
  
  )
}

export default About