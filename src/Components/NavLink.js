export  const links=[
    {
        id: 1,
        link: 'home'   
    },
    {
        id: 2,
        link: 'about'
    },
    {
        id: 3,
        link: 'services'
    },
    {
        id: 4,
        link: 'contact'
    },
    {
        id: 5,
        link: 'gallery'
    },
] 

export  const ylink=[
    {
        id: 1,
        link: 'https://www.youtube.com/embed/Gx7IXd7DSKk'   
    },
    {
        id: 2,
        link: 'https://www.youtube.com/embed/SeoL3v7i4zk'
    },
    {
        id: 3,
        link: 'https://www.youtube.com/embed/qMisuN1-YmY'
    },
    {
        id: 4,
        link: 'https://www.youtube.com/embed/rzMMsOzOuLo'
    },
    {
        id: 5,
        link: 'https://www.youtube.com/embed/TB3kxjz1r1I'
    },
    {
        id: 6,
        link: 'https://www.youtube.com/embed/ZOJctbLqxyQ'
    },
]