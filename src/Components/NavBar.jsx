import React from 'react'
import { useState } from "react";
import { FaTimes} from "react-icons/fa"
import {MdMenuOpen} from "react-icons/md"
import { Link } from 'react-scroll';
import {links} from './NavLink'
import SideNav from './SideNav';
 const NavBar = () => {
    const [nav,setNav]= useState(false)
   
  return (
    <div className='flex justify-between items-center w-full h-20 px-4 text-white bg-black fixed' >
        <div onClick={()=>setNav(!nav)} className='md:hidden cursor-pointer pr-4 z-10 text-gray-500'>
            {nav ? <FaTimes size={30}/> :<MdMenuOpen size={30}/>}
        </div>
        <div>
            <h1 className='text-5xl font-signature ml-2 cursor-pointer'><Link to="home" smooth duration={500}>Anoop Sivakumar</Link></h1>
        </div>
        <ul className='hidden md:flex'>
            {links.map(({id,link}) =>(
         <li key={id} className='px-4 cursor-pointer capitalize font-medium text-gray-500 hover:scale-105 duration-200'><Link to={link} smooth duration={500}>{link}</Link></li>
            ))}
        </ul>

            {
            nav && (<SideNav/>)
            }

    </div>
  )
}

export default NavBar